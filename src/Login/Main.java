package Login;

import Network.Connection;
import Network.Database;
import MainFeed.MainFeed;
import Register.Register;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import javafx.scene.input.MouseEvent;
import java.io.FileInputStream;

public class Main extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        String localDir = System.getProperty("user.dir");

        stage.setTitle("Math Challenge");

        DropShadow dropShadow = new DropShadow();
        BorderPane borderPane = new BorderPane();
        Image image = new Image(new FileInputStream(localDir + "/src/image/backgroundImage.png"));
        Image bgInside = new Image(new FileInputStream(localDir + "/src/image/Inside.png"));
        Image underlinePic = new Image(new FileInputStream(localDir + "/src/image/whiteUnderline.PNG"));

        BackgroundImage backgroundImage = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        Background background = new Background(backgroundImage);
        borderPane.setBackground(background);

        VBox vBox = new VBox();
        VBox buttonPlane = new VBox();
        vBox.setSpacing(40);

        Label label = new Label("Welcome to Math Challenge");
        Label label2 = new Label("Everything you need to practice your\nmathematics skills");
        Label loginStatusLabel = new Label("");

        label2.setTextFill(Color.WHITE);
        label.setTextFill(Color.WHITE);
        loginStatusLabel.setTextFill(Color.RED);

        label.setFont(Font.font("Times New Roman", FontWeight.BOLD, 40));
        label2.setFont(Font.font("Times New Roman", FontWeight.BOLD, 30));
        loginStatusLabel.setFont(Font.font("Times New Roman", FontWeight.BOLD, 20));

        vBox.getChildren().addAll(label);
        vBox.getChildren().addAll(label2);


        Button signUpButton = new Button("Register now");
        signUpButton.setPrefWidth(220);
        signUpButton.setPrefHeight(50);
        signUpButton.setStyle("-fx-background-color: #1DA0F1");
        signUpButton.setFont(Font.font("Times New Roman", FontWeight.BOLD, 30));

        // set gridPane position for X and Y
        vBox.setTranslateX(50);
        vBox.setTranslateY(260);
        buttonPlane.getChildren().add(signUpButton);
        VBox.setMargin(buttonPlane, new Insets(5, 0, 0, 100));
        vBox.getChildren().add(buttonPlane);

        borderPane.setLeft(vBox);
        BorderPane.setMargin(vBox, new Insets(-110, 0, 0, 0));

        StackPane edge = new StackPane();
        edge.setPrefWidth(500);
        edge.setStyle("-fx-background-color: #028EFC;");

        Label inside = new Label();

        ImageView backgroundInside = new ImageView(bgInside);
        backgroundInside.setFitHeight(490);
        backgroundInside.setFitWidth(490);

        inside.setGraphic(backgroundInside);

        StackPane.setMargin(inside, new Insets(5, 0, 5, 0));

        edge.getChildren().add(inside);

        BorderPane login = new BorderPane();
        Label loginLabel = new Label("Login");

        loginLabel.setTextFill(Color.WHITE);
        loginLabel.setFont(Font.font("Times New Roman", FontWeight.BOLD, 40));

        BorderPane.setAlignment(loginLabel, Pos.TOP_CENTER);
        BorderPane.setMargin(loginLabel, new Insets(50, 0, 0, 0));

        login.setTop(loginLabel);

        Button signInButton = new Button("Sign In");

        signInButton.setPrefWidth(350);
        signInButton.setPrefHeight(50);
        signInButton.setStyle("-fx-background-color: #1DA0F1");
        signInButton.setFont(Font.font("Times New Roman", FontWeight.BOLD, 30));

        edge.getChildren().add(login);

        GridPane inputs = new GridPane();
        TextField username1 = new TextField();
        username1.setPromptText("username");
        PasswordField password1 = new PasswordField();
        password1.setPromptText("password");

        username1.setStyle("-fx-prompt-text-fill: white; -fx-background-color: transparent; -fx-text-fill: #FFFFFF");
        username1.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        username1.setPrefHeight(30);

        password1.setStyle("-fx-prompt-text-fill: white; -fx-background-color: transparent; -fx-text-fill: #FFFFFF");
        password1.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        password1.setPrefHeight(30);

        Label underline = new Label();
        Label underline2 = new Label();

        ImageView underlineImage = new ImageView(underlinePic);
        underlineImage.setFitHeight(5);
        underlineImage.setFitWidth(350);
        underline.setGraphic(underlineImage);

        ImageView underlineImage2 = new ImageView(underlinePic);
        underlineImage2.setFitHeight(5);
        underlineImage2.setFitWidth(350);
        underline2.setGraphic(underlineImage2);

        GridPane.setMargin(underline, new Insets(0, 0, 0, 80));
        GridPane.setMargin(username1, new Insets(70, 0, 0, 80));
        GridPane.setMargin(password1, new Insets(40, 0, 0, 80));
        GridPane.setMargin(underline2, new Insets(0, 0, 0, 80));
        GridPane.setMargin(signInButton, new Insets(30, 0, 0, 80));
        GridPane.setMargin(loginStatusLabel, new Insets(10, 0, 0, 180));

        inputs.add(username1, 0, 0);
        inputs.add(underline, 0, 1);
        inputs.add(password1, 0, 2);
        inputs.add(underline2, 0, 3);
        inputs.add(signInButton, 0, 4);
        inputs.add(loginStatusLabel, 0, 5);

        login.setLeft(inputs);

        BorderPane.setMargin(edge, new Insets(70, 100, 150, 0));
        borderPane.setRight(edge);

        signInButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                String token = Database.loginAccount(username1.getText(), password1.getText());

                if ((token == null)) {
                    loginStatusLabel.setText("Authentication Error");

                    username1.clear();
                    password1.clear();

                    return;
                }

                MainFeed mainfeed = new MainFeed();
                mainfeed.setToken(token);
                mainfeed.setUsername(username1.getText());

                try {
                    mainfeed.start(stage);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                username1.clear();
                password1.clear();
            }
        });

        Scene scene = new Scene(borderPane, 1200, 650);
        stage.setScene(scene);

        signUpButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                try {
                    Register registerWindow = new Register();
                    registerWindow.start(stage);
                } catch (Exception e){
                    System.out.println(e.getMessage());
                }
            }
        });

        signInButton.addEventHandler(MouseEvent.MOUSE_ENTERED,
                new EventHandler<MouseEvent>() {
                    @Override public void handle(MouseEvent e) {
                        signInButton.setEffect(dropShadow);
                    }
                });

        signInButton.addEventHandler(MouseEvent.MOUSE_EXITED,
                new EventHandler<MouseEvent>() {
                    @Override public void handle(MouseEvent e) {
                        signInButton.setEffect(null);
                    }
                });

        signUpButton.addEventHandler(MouseEvent.MOUSE_ENTERED,
                new EventHandler<MouseEvent>() {
                    @Override public void handle(MouseEvent e) {
                        signUpButton.setEffect(dropShadow);
                    }
                });

        signUpButton.addEventHandler(MouseEvent.MOUSE_EXITED,
                new EventHandler<MouseEvent>() {
                    @Override public void handle(MouseEvent e) {
                        signUpButton.setEffect(null);
                    }
                });

        stage.show();
    }

    public static void main(String[] args) {
        Connection.callDatabase();
        launch(args);
    }
}
