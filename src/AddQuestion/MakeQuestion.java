package AddQuestion;

import Register.Register;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class MakeQuestion extends Application {

    private double WIDTH = 1200;
    private double HEIGHT = 650;
    private static int pageNum = 1;
    private String localDir = System.getProperty("user.dir");

    @Override
    public void start(Stage stage) throws Exception {

        Image backgroundImage = new Image(new FileInputStream( localDir + "/src/image/joinGameBg.png" ) );
        ImageView imageViewBackGround = new ImageView(backgroundImage);
        imageViewBackGround.setFitHeight( HEIGHT );
        imageViewBackGround.setFitWidth( WIDTH );

        Label questionLabel = new Label();
        questionLabel.setText("Question " + pageNum );
        questionLabel.setStyle("-fx-effect: dropshadow(three-pass-box, black, 10, 0, 0, 0);");
        questionLabel.setFont(Font.font("Times New Roman", FontWeight.BOLD, 50));
        questionLabel.setTextFill( Color.web("white") );

        TextField questionTitleTextField = new TextField("");
        questionTitleTextField.setPromptText( "Question" + pageNum );
        questionTitleTextField.setFont(Font.font("Verdana", FontWeight.BOLD, 25));
        questionTitleTextField.setStyle("-fx-prompt-text-fill: #A9A9A9;");

        Button addQuizButton = new Button("Add Question");
        addQuizButton.setStyle("-fx-font-size: 25px; -fx-background-color: #3d85c6; -fx-text-fill: #ffffff ;" +
                "-fx-font-family: Verdana;");

        LableAndButton labelButton1 = new LableAndButton();
        LableAndButton labelButton2 = new LableAndButton();
        LableAndButton labelButton3 = new LableAndButton();
        LableAndButton labelButton4 = new LableAndButton();

        HBox row1 = new HBox();
        row1.getChildren().add( labelButton1.getButtonAndLabel() );
        row1.getChildren().add( labelButton2.getButtonAndLabel() );

        HBox row2 = new HBox();
        row2.getChildren().add( labelButton3.getButtonAndLabel() );
        row2.getChildren().add( labelButton4.getButtonAndLabel() );

        VBox allWindow = new VBox();
        allWindow.getChildren().add( questionLabel );
        allWindow.getChildren().add( questionTitleTextField );
        allWindow.getChildren().add( row1 );
        allWindow.getChildren().add( row2 );
        allWindow.getChildren().add( addQuizButton );

        StackPane stackPane = new StackPane();
        stackPane.getChildren().addAll( imageViewBackGround , allWindow );

        HBox mainScreen = new HBox();
        mainScreen.getChildren().add( stackPane );
        Scene scene = new Scene( mainScreen , WIDTH, HEIGHT);
        stage.setTitle("Make Question");
        stage.setScene(scene);
        stage.show();

    }

    class LableAndButton extends HBox {
        public int change = 1;

        private Image checkImage = new Image(new FileInputStream(localDir + "/src/image/check.png"));
        private ImageView checkViewBackGround = new ImageView(checkImage);

        private Image croseImage = new Image(new FileInputStream(localDir + "/src/image/crose.jpg"));
        private ImageView croseViewBackGround = new ImageView( croseImage );

        private Button createQuizButton = new Button("");
        private  TextField questionTextField = new TextField();

        public LableAndButton() throws FileNotFoundException {

            checkViewBackGround.setFitHeight( 30 );
            checkViewBackGround.setFitWidth( 30 );

            croseViewBackGround.setFitHeight( 30 );
            croseViewBackGround.setFitWidth( 30 );

            createQuizButton.setGraphic( croseViewBackGround );
            createQuizButton.setFont(Font.font("Times New Roman", FontWeight.BOLD, 40));

            questionTextField.setFont(Font.font("Times New Roman", FontWeight.BOLD, 40));

            createQuizButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    if ( change == 1) {
                        createQuizButton.setGraphic(croseViewBackGround);
                        change = 0;
                    }else {
                        createQuizButton.setGraphic(checkViewBackGround);
                        change = 1;
                    }
                }
            });
        }
        public  HBox getButtonAndLabel(){
            HBox box = new HBox();
            box.getChildren().add( questionTextField );
            box.getChildren().add( createQuizButton );
            return box;
        }

    }



    public static void main(String[] args) {
        Application.launch(args);
    }

}
