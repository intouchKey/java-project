package MainFeed;

import JoinGame.JoinGame;
import HostGame.HostGame;
import Model.UserInfo;
import Network.Database;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.util.Stack;

import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class MainFeed extends Application {

    public int HEIGHT = 650;
    public int WIDTH = 1200;
    private String username;
    private String token;

    @Override
    public void start(Stage stage) throws Exception {
        String localDir = System.getProperty("user.dir");

        UserInfo userInfo = Database.getUserInfo(username, token);

        Image backgroundImage = new Image(new FileInputStream( localDir + "/src/image/mainFeedBackground.png" ) );
        ImageView imageViewBackGround = new ImageView(backgroundImage);
        imageViewBackGround.setFitHeight( HEIGHT );
        imageViewBackGround.setFitWidth( WIDTH );

        Image botPlayImage = new Image(new FileInputStream( localDir + "/src/image/joinGameButtonPic.png" ) );
        ImageView botPlayViewBackGround = new ImageView(botPlayImage);
        imageViewBackGround.setFitHeight( HEIGHT );
        imageViewBackGround.setFitWidth( WIDTH );

        Image withFriendImage = new Image(new FileInputStream( localDir + "/src/image/hostGameButtonPic.png" ) );
        ImageView withFriendViewBackGround = new ImageView(withFriendImage);
        imageViewBackGround.setFitHeight( HEIGHT );
        imageViewBackGround.setFitWidth( WIDTH );

        StackPane profileEdge = new StackPane();
        profileEdge.setPrefWidth(300);
        profileEdge.setPrefHeight(10);
        profileEdge.setTranslateX(50);

        profileEdge.setStyle("-fx-background-color: #404040;");

        HBox profile = new HBox();

        Label leftEdge = new Label();
        Image greenEdge = new Image(new FileInputStream( localDir + "/src/image/leftyEdge.png" ) );
        ImageView edgeImage = new ImageView(greenEdge);
        edgeImage.setFitHeight(55);
        edgeImage.setFitWidth(20);
        leftEdge.setGraphic(edgeImage);

        StackPane levelStack = new StackPane();
        levelStack.setPrefWidth(55);
        levelStack.setStyle("-fx-background-color: #FFFFFF");


        String levelNumber = Integer.toString(userInfo.getLevel());
        Label level = new Label(levelNumber);
        level.setFont(Font.font("Times New Roman", 30));
        levelStack.getChildren().add(level);
        StackPane.setAlignment(level, Pos.CENTER);

        VBox nameVBox = new VBox();
        nameVBox.setSpacing(3);
        Label name = new Label((userInfo.getUsername()));
        name.setTextFill(Color.WHITE);
        name.setFont(Font.font("Times New Roman", FontWeight.BOLD, 30));
        nameVBox.setPrefWidth(225);
        nameVBox.getChildren().add(name);
        nameVBox.setAlignment(Pos.CENTER);


        VBox bigLevelBar = new VBox();
        bigLevelBar.setPrefWidth(225);

        StackPane levelBar = new StackPane();

        Label whiteBar = new Label();
        Image whiteBarImage = new Image(new FileInputStream(localDir + "/src/image/UserExperience.png"));
        ImageView whiteBarView = new ImageView(whiteBarImage);
        whiteBarView.setFitHeight(5);
        whiteBarView.setFitWidth(200);
        whiteBar.setGraphic(whiteBarView);

        double length = userInfo.getExperience() * 300 / 200;
        ProgressBar pb = new ProgressBar();
        pb.setPrefWidth(length);
        pb.setPrefHeight(5);
        levelBar.setAlignment(Pos.CENTER_LEFT);
        levelBar.getChildren().add(whiteBar);
        levelBar.getChildren().add(pb);
        whiteBar.setTranslateX(0);
        pb.setTranslateX(0);

        VBox.setMargin(levelBar, new Insets(0, 0, 0, 10));


//        nameVBox.getChildren().add(levelBar);
        bigLevelBar.getChildren().add(levelBar);

        profileEdge.getChildren().add(nameVBox);

        nameVBox.getChildren().add(bigLevelBar);

        profile.getChildren().add(leftEdge);
        profile.getChildren().add(levelStack);
        profile.getChildren().add(nameVBox);
//        profile.getChildren().add(nameStack);

        profileEdge.getChildren().add(profile);

        Button quickPlayButton = new Button();
        quickPlayButton.setPrefWidth(400);
        quickPlayButton.setPrefHeight(200);
        BackgroundImage joinGameBackgroundImage = new BackgroundImage(botPlayImage, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        Background joinGameBackground = new Background(joinGameBackgroundImage);
        quickPlayButton.setBackground(joinGameBackground);

        Button botPlayButton = new Button();
        botPlayButton.setPrefWidth(400);
        botPlayButton.setPrefHeight(200);
        BackgroundImage withFriendBackgroundImage = new BackgroundImage(withFriendImage, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        Background withFriendBackground = new Background(withFriendBackgroundImage);
        botPlayButton.setBackground(withFriendBackground);

        VBox leftScreenSide = new VBox();

        Label whiteSpace = new Label();
        Image whiteImage = new Image(new FileInputStream(localDir + "/src/image/whiteSpace.png"));
        ImageView whiteSpaceImage = new ImageView(whiteImage);
        whiteSpace.setGraphic(whiteSpaceImage);

        leftScreenSide.getChildren().add(whiteSpace);
        leftScreenSide.getChildren().addAll( quickPlayButton );

        leftScreenSide.setTranslateX( 150 );
        leftScreenSide.setTranslateY( 125 );

        VBox rightScreenSide = new VBox();

        Label rightWhiteSpace = new Label();
        Image rightWhiteImage = new Image(new FileInputStream(localDir + "/src/image/whiteSpace.png"));
        ImageView rightWhiteSpaceImage = new ImageView(rightWhiteImage);
        rightWhiteSpace.setGraphic(rightWhiteSpaceImage);

        rightScreenSide.getChildren().add(rightWhiteSpace);
        rightScreenSide.getChildren().addAll( botPlayButton );
        rightScreenSide.setTranslateX( 300 );
        rightScreenSide.setTranslateY( 125 );

        HBox allWindow = new HBox();
        allWindow.getChildren().addAll( leftScreenSide );
        allWindow.getChildren().addAll( rightScreenSide );
        allWindow.getChildren().addAll( profileEdge );

        HBox.setMargin(profileEdge, new Insets(20, 0, 575, 0));

        StackPane stackPane = new StackPane();
        stackPane.getChildren().addAll( imageViewBackGround , allWindow );

        HBox mainScreen = new HBox();
        mainScreen.getChildren().add( stackPane );

        Scene scene = new Scene( mainScreen , 1200, 650);

        stage.setTitle("Quick Math");
        stage.setScene(scene);
        stage.show();

        quickPlayButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                try {
                    JoinGame joinGameWindow = new JoinGame();
                    joinGameWindow.start(stage);
                } catch (Exception e){
                    System.out.println(e.getMessage());
                }
            }
        });

        botPlayButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                try {
                    HostGame hostGame = new HostGame();
                    hostGame.start(stage);
                } catch (Exception e){
                    System.out.println(e.getMessage());
                }
            }
        });
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setToken(String token) {
        this.token = token;
    }
}