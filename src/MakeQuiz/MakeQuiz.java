package MakeQuiz;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.io.FileInputStream;

public class MakeQuiz extends Application {

    private double WIDTH = 1200;
    private double HEIGHT = 650;

    @Override
    public void start(Stage stage) throws Exception {
        String localDir = System.getProperty("user.dir");

        Image backgroundImage = new Image(new FileInputStream( localDir + "/src/image/joinGameBg.png" ) );
        ImageView imageViewBackGround = new ImageView(backgroundImage);
        imageViewBackGround.setFitHeight( HEIGHT );
        imageViewBackGround.setFitWidth( WIDTH );

        Label makeQuestionLabel = new Label();
        makeQuestionLabel.setText("Make your question and answer to play with \n\t\t\t      your friend\n");
        makeQuestionLabel.setStyle("-fx-effect: dropshadow(three-pass-box, black, 10, 0, 0, 0);");
        makeQuestionLabel.setFont(Font.font("Times New Roman", FontWeight.BOLD, 50));
        makeQuestionLabel.setTextFill( Color.web("white") );
        makeQuestionLabel.setTranslateY( -200 );
        makeQuestionLabel.setTranslateX(  -200 );

        TextField quiaTitle = new TextField("");
        quiaTitle.setPromptText( "Your Quiz Title" );
        quiaTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 25));
        quiaTitle.setStyle("-fx-prompt-text-fill: #A9A9A9;");
        quiaTitle.setAlignment(Pos.CENTER);
        quiaTitle.setTranslateY( -70 );
        quiaTitle.setTranslateX( 30 );
        quiaTitle.setMaxWidth( 500 );

        Button createQuiz = new Button("Create Quiz");
        createQuiz.setStyle("-fx-font-size: 25px; -fx-background-color: #3d85c6; -fx-text-fill: #ffffff ;" +
                "-fx-font-family: Verdana;");
        createQuiz.setTranslateY( 40 );
        createQuiz.setTranslateX( 120 );
        createQuiz.setMaxWidth(300);

        VBox textFieldGroup = new VBox();
        textFieldGroup.getChildren().addAll( quiaTitle );
        textFieldGroup.setTranslateY( -10 );

        VBox allWindow = new VBox();
        allWindow.getChildren().addAll( makeQuestionLabel );
        allWindow.getChildren().addAll( textFieldGroup );
        allWindow.getChildren().addAll( createQuiz );
        allWindow.setTranslateX( 350 );
        allWindow.setTranslateY( 255 );

        StackPane stackPane = new StackPane();
        stackPane.getChildren().addAll( imageViewBackGround , allWindow );

        HBox mainScreen = new HBox();
        mainScreen.getChildren().add( stackPane );
        Scene scene = new Scene( mainScreen , WIDTH, HEIGHT);

        stage.setTitle("Make Question");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }

}
