package Network;

import Model.UserInfo;
import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.net.URI;

import static Network.Connection.getResponseMessage;

public class Database {
    public static String loginAccount(String username, String password) {
        try {
            String url = "https://java-math-challenge.herokuapp.com/authentication/login";
            String payload = "{\"username\":"+ " \""+ username + "\"," +"\"password\":" +" \""+password+"\"}";

            HttpClient client = HttpClients.custom().build();
            HttpUriRequest request = RequestBuilder.get()
                    .setUri(url)
                    .setEntity(new StringEntity(payload , ContentType.APPLICATION_JSON))
                    .setHeader("Content-Type", "application/json")
                    .build();
            HttpResponse response = client.execute(request);

            if (response.getStatusLine().getStatusCode() != 200) {
                return null;
            }

            String token = getResponseMessage(response);

            return token;
        } catch (Exception e) {
            return null;
        }
    }

    public static String registerAccount(String username, String password) {
        try {
            String url = "https://java-math-challenge.herokuapp.com/authentication/register";
            String payload = "{\"username\":"+ " \""+ username + "\"," +"\"password\":" +" \""+password+"\"}";

            HttpClient client = HttpClients.custom().build();
            HttpUriRequest request = RequestBuilder.post()
                    .setUri(url)
                    .setEntity(new StringEntity(payload , ContentType.APPLICATION_JSON))
                    .setHeader("Content-Type", "application/json")
                    .build();
            HttpResponse response = client.execute(request);

            if (response.getStatusLine().getStatusCode() != 200) {
                return null;
            }

            String token = getResponseMessage(response);

            return token;
        } catch (Exception e) {
            return null;
        }
    }

    public static UserInfo getUserInfo(String username, String token) {
        try {
            String url = "https://java-math-challenge.herokuapp.com/user/get_info";

            HttpClient client = HttpClients.custom().build();
            HttpUriRequest request = RequestBuilder.get()
                    .setUri(url)
                    .setHeader("username", username)
                    .setHeader("Authorization", "Bearer " + token)
                    .build();
            HttpResponse response = client.execute(request);

            UserInfo userInfo = new Gson().fromJson(getResponseMessage(response), UserInfo.class);

            return userInfo;
        } catch (Exception e) {
            return null;
        }
    }
}
