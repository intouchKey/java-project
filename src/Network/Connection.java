package Network;

import javafx.scene.control.Alert;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Connection {
    public static String getResponseMessage(HttpResponse response) {
        StringBuffer result = new StringBuffer();

        try {
            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));


            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }

        return result.toString();
    }

    public static void callDatabase() {
        try {
            String url = "https://java-math-challenge.herokuapp.com/";

            HttpClient client = new DefaultHttpClient();
            HttpGet get = new HttpGet(url);

            client.execute(get);;
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setContentText("No internet connection");
            alert.showAndWait();

            System.exit(0);
        }
    }

}
