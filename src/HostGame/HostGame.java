package HostGame;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.io.FileInputStream;

public class HostGame extends Application {

    private double WIDTH = 1200;
    private double HEIGHT = 650;

    @Override
    public void start(Stage stage) throws Exception {
        String localDir = System.getProperty("user.dir");

        Image backgroundImage = new Image(new FileInputStream( localDir + "/src/image/joinGameBg.png" ) );
        ImageView imageViewBackGround = new ImageView(backgroundImage);
        imageViewBackGround.setFitHeight( HEIGHT );
        imageViewBackGround.setFitWidth( WIDTH );

        Label roomInfoLabel = new Label();
        roomInfoLabel.setText(" Enter room’s information to host the game !\n");
        roomInfoLabel.setStyle("-fx-effect: dropshadow(three-pass-box, black, 10, 0, 0, 0);");
        roomInfoLabel.setFont(Font.font("Times New Roman", FontWeight.BOLD, 30));
        roomInfoLabel.setTextFill( Color.web("white") );
        roomInfoLabel.setTranslateY( -30);

        TextField rooomNameTextField = new TextField("");
        rooomNameTextField.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        rooomNameTextField.setStyle("-fx-prompt-text-fill: #A9A9A9;");
        rooomNameTextField.setAlignment(Pos.CENTER);
        rooomNameTextField.setPromptText( "room's name" );
        rooomNameTextField.setTranslateX( 30 );
        rooomNameTextField.setMaxHeight( 50 );
        rooomNameTextField.setMaxWidth( 500 );

        TextField roomPasswordTextField = new TextField("");
        roomPasswordTextField.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        roomPasswordTextField.setStyle("-fx-prompt-text-fill: #A9A9A9;");
        roomPasswordTextField.setAlignment(Pos.CENTER);
        roomPasswordTextField.setPromptText( "room's password" );
        roomPasswordTextField.setTranslateX( 30 );
        roomPasswordTextField.setTranslateY( 10 );
        roomPasswordTextField.setMaxHeight( 20 );
        roomPasswordTextField.setMaxWidth( 500 );

        Button joinRoomButton = new Button("Host game");
        joinRoomButton.setStyle("-fx-font-size: 18px; -fx-background-color: #3d85c6; -fx-text-fill: #ffffff ;" +
                                "-fx-font-family: Verdana;");
        joinRoomButton.setTranslateY( 17 );
        joinRoomButton.setTranslateX( 120 );
        joinRoomButton.setMaxWidth(300);

        VBox textFieldGroup = new VBox();
        textFieldGroup.getChildren().addAll( rooomNameTextField );
        textFieldGroup.getChildren().addAll( roomPasswordTextField );
        textFieldGroup.setTranslateY( -10 );

        VBox allWindow = new VBox();
        allWindow.getChildren().addAll( roomInfoLabel );
        allWindow.getChildren().addAll( textFieldGroup );
        allWindow.getChildren().addAll( joinRoomButton );
        allWindow.setTranslateX( 350 );
        allWindow.setTranslateY( 255 );

        StackPane stackPane = new StackPane();
        stackPane.getChildren().addAll( imageViewBackGround , allWindow );

        HBox mainScreen = new HBox();
        mainScreen.getChildren().add( stackPane );
        Scene scene = new Scene( mainScreen , WIDTH, HEIGHT);

        stage.setTitle("Host Game");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }

}