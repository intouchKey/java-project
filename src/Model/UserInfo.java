package Model;

public class UserInfo {
    private String username;
    private int level;
    private int experience;
    private int games_played;

    public String getUsername() { return username; }
    public int getLevel() { return level; }
    public int getExperience() { return experience; }
    public int getGamesPlayed() { return games_played; }
}
